#!/bin/bash

# 
# Example script
# 
# Author Vit Kabele <vit@kabele.me>
#

OUTPUT_DIR=output
DATA_DIR=data

if [ ! -d "$OUTPUT_DIR" ];then
    echo "The directory $OUTPUT_DIR is not present";
else
    echo "The directory $OUTPUT_DIR is present";
fi


if [ ! -d "$DATA_DIR" ];then
    echo "The directory $DATA_DIR is not present";
else
    echo "The directory $DATA_DIR is present";
    ls -l "$DATA_DIR"
fi
