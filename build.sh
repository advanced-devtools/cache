#!/bin/bash

# 
# Example script
# 
# Author Vit Kabele <vit@kabele.me>
#

OUT_DIR=output

mkdir "$OUT_DIR" || exit 1;


for I in $(seq 5);do
    touch "$OUT_DIR/file$I";
done
