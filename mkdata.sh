#!/bin/bash

# 
# Example script
# 
# Author Vit Kabele <vit@kabele.me>
#

DATA_DIR=data

if [ -z "$1" ];then
    echo "Please provide valid name for data";
    exit 1;
fi

if [ ! -d "$DATA_DIR" ];then
    mkdir $DATA_DIR || exit 1;
fi

touch "$DATA_DIR/$1.txt"

